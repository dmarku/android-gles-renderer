package com.example.gl;

import javax.microedition.khronos.opengles.GL10;

public interface RenderView {
	public void resize(float width, float height);
	public void draw(GL10 gl);
}
