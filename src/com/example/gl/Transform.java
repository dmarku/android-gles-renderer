package com.example.gl;

public interface Transform {
	public float[] getMatrix();
}
