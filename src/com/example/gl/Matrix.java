package com.example.gl;

/** Column-major 4x4 matrix representation and utility functions */
public class Matrix {
	private float[] mValues;
	
	public Matrix() {
		mValues = new float[] {
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1
		};
	}
	
	public float[] getValues() {
		return mValues;
	}
	
	public static float[] set(float[] matrix, Vector x, Vector y, Vector z, Vector origin)
			throws IllegalArgumentException {
		
		if(matrix.length != 16)
			throw new IllegalArgumentException("matrix array must consist of 16 elements");
		
		float[] values = x.getValues();
		matrix[0] = values[0];
		matrix[1] = values[1];
		matrix[2] = values[2];
		matrix[3] = values[3];
		
		values = y.getValues();
		matrix[4] = values[0];
		matrix[5] = values[1];
		matrix[6] = values[2];
		matrix[7] = values[3];
		
		values = z.getValues();
		matrix[8] = values[0];
		matrix[9] = values[1];
		matrix[10] = values[2];
		matrix[11] = values[3];
		
		values = origin.getValues();
		matrix[12] = values[0];
		matrix[13] = values[1];
		matrix[14] = values[2];
		matrix[15] = values[3];
		
		return matrix;
	}
	
	public void setX(float[] xValues) throws IllegalArgumentException {
		if(xValues.length != 3)
			throw new IllegalArgumentException("number of values must be 3");
		mValues[0] = xValues[0];
		mValues[1] = xValues[1];
		mValues[2] = xValues[2];
//		mValues[3] = xValues[3];
	}
	
	public void setY(float[] yValues) throws IllegalArgumentException {
		if(yValues.length != 3)
			throw new IllegalArgumentException("number of values must be 3");
		mValues[4] = yValues[0];
		mValues[5] = yValues[1];
		mValues[6] = yValues[2];
//		mValues[7] = yValues[3];
	}
	
	public void setZ(float[] zValues) throws IllegalArgumentException {
		if(zValues.length != 3)
			throw new IllegalArgumentException("number of values must be 3");
		mValues[8] = zValues[0];
		mValues[9] = zValues[1];
		mValues[10] = zValues[2];
//		mValues[11] = zValues[3];
	}
	
	public void setOrigin(float[] origin) throws IllegalArgumentException {
		if(origin.length != 3)
			throw new IllegalArgumentException("number of values must be 3");
		mValues[12] = origin[0];
		mValues[13] = origin[1];
		mValues[14] = origin[2];
//		mValues[15] = origin[3];
	}
}
