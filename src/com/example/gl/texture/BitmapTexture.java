package com.example.gl.texture;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;
import android.opengl.GLUtils;

import com.example.gl.Texture;

public class BitmapTexture implements Texture {

	private boolean mIsInitialized = false;
	private int mTextureId = 0;
	private boolean mBitmapPending = false;
	private Bitmap mPendingBitmap = null;
	private Bitmap mSourceBitmap = null;
	
	public BitmapTexture() {
	}
	
	public BitmapTexture(Bitmap bitmap) {
		setBitmap(bitmap);
	}
	
	public void setBitmap(Bitmap bitmap) {
		mPendingBitmap = bitmap;
		mBitmapPending = true;
	}

	public void loadBitmap(GL10 gl, Bitmap bitmap) {
		if(!mIsInitialized) {
			init(gl);
		}
		else {
			gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureId);
		}
		
		if( bitmap != null ) {
			if( mSourceBitmap == null ||
					bitmap.getWidth() != mSourceBitmap.getWidth() ||
					bitmap.getHeight() != mSourceBitmap.getHeight() ) {
				GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);
			}
			else {
				GLUtils.texSubImage2D(GL10.GL_TEXTURE_2D, 0, 0, 0, bitmap);
			}
		}
		mSourceBitmap = bitmap;
	}
	
	public void init(GL10 gl) {
		int[] textures = new int[1];
		gl.glGenTextures(1, textures, 0);
		mTextureId = textures[0];
		
		gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureId);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_S, GL10.GL_REPEAT);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_WRAP_T, GL10.GL_REPEAT);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR);
		gl.glTexParameterx(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_LINEAR);
		mIsInitialized = true;
	}
	
	@Override
	public void use(GL10 gl) {
		if(mBitmapPending) {
			loadBitmap(gl, mPendingBitmap);
			mBitmapPending = false;
		}
		else {
			gl.glBindTexture(GL10.GL_TEXTURE_2D, mTextureId);
		}
	}

}
