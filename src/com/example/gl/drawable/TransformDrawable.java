package com.example.gl.drawable;

import javax.microedition.khronos.opengles.GL10;

import com.example.gl.Drawable;
import com.example.gl.Transform;

public class TransformDrawable implements Drawable {
	
	private Transform mTransform = null;
	private Drawable mChildDrawable = null;
	
	public TransformDrawable(Transform transform, Drawable childDrawable) {
		setTransform(transform);
		setChildDrawable(childDrawable);
	}
	
	public TransformDrawable(Drawable childDrawable) {
		setChildDrawable(childDrawable);
	}
	
	public TransformDrawable() {
	}

	public void setTransform(Transform transform) {
		mTransform = transform;
	}
	
	public void setChildDrawable(Drawable childDrawable) {
		mChildDrawable = childDrawable;
	}
	
	@Override
	public void draw(GL10 gl) {
		if(mChildDrawable != null) {
			gl.glPushMatrix();
			
			if(mTransform != null)
				gl.glMultMatrixf(mTransform.getMatrix(), 0);
			
			mChildDrawable.draw(gl);
			
			gl.glPopMatrix();
		}

	}

}
