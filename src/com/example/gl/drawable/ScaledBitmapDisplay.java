package com.example.gl.drawable;

import android.graphics.Bitmap;
import android.util.Log;

public class ScaledBitmapDisplay extends BitmapDisplay {
	private static final String TAG = "ScaledBitmapDisplay";
	
	private float mBitmapWidth = 1, mBitmapHeight = 1;
	private float mLeft = -1, mRight = 1, mBottom = -1, mTop = 1;
	
	private boolean mBitmapInitialized = false;
	private boolean mAreaInitialized = false;
	
	public ScaledBitmapDisplay(Bitmap bitmap) {
		super(bitmap);
	}

	/**
	 * Sets the Bitmap to use for display and corrects scaling if neccessary.
	 */
	@Override
	public void setBitmap(Bitmap bitmap) {
		super.setBitmap(bitmap);
		if(bitmap != null) {
			// TODO: avoid recalculation on unchanged bitmap / unchanged size
			mBitmapWidth = (float) bitmap.getWidth();
			mBitmapHeight = (float) bitmap.getHeight();
			Log.d(TAG, "Bitmap set, size "+mBitmapWidth+"x"+mBitmapHeight);
			mBitmapInitialized = true;
			if(mAreaInitialized)
				recalc();
		}
	}
	
	@Override
	public void setArea(float left, float right, float bottom, float top) {
		// TODO: avoid recalculation on unchanged area
		mLeft = left; mRight = right;
		mBottom = bottom; mTop = top;
		Log.d(TAG, "Area set, ["+mLeft+", "+mRight+"] x ["+mBottom+", "+mTop+"]");
		
		mAreaInitialized = true;
		if(mBitmapInitialized)
			recalc();
	}
	
	/**
	 * Recalculates geometry/texture coordinates when bitmap or display area
	 * have changed.
	 */
	public void recalc() {
		Log.d(TAG, "Recalculating dimensions...");
		Log.d(TAG, "Bitmap has size "+mBitmapWidth+"x"+mBitmapHeight);
		Log.d(TAG, "Area is, ["+mLeft+", "+mRight+"] x ["+mBottom+", "+mTop+"]");
		float areaWidth = mRight - mLeft;
		float areaHeight = mTop - mBottom;
		
		float left = mLeft, right = mRight, bottom = mBottom, top = mTop;
		
		// bitmap aspect > area aspect
		if(mBitmapWidth*areaHeight > mBitmapHeight*areaWidth) {
			float scale = mBitmapWidth*areaHeight / (areaWidth*mBitmapHeight);
			left = scale * mLeft;
			right = scale * mRight;
			Log.d(TAG, "Recalculating: adjusting left and right boundaries");
		}
		else {
			float scale = mBitmapHeight*areaWidth / (areaHeight*mBitmapWidth);
			bottom = scale * mBottom;
			top = scale * mTop;
			Log.d(TAG, "Recalculating: adjusting top and bottom boundaries");
		}
		
		super.setArea(left, right, bottom, top);
		Log.d(TAG, "Boundaries calculated: ["+left+", "+right+"] x ["+bottom+", "+top+"]");
//		super.setArea(-2, 2, -2, 2); // only for testing purposes
	}
}
