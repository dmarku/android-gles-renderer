package com.example.gl.drawable;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.opengles.GL10;

import com.example.gl.Drawable;

/** Compound Object that draws a number of child Drawables */
public class DrawableBundle implements Drawable {
	private List<Drawable> mChildren;
	
	public DrawableBundle() {
		mChildren = new ArrayList<Drawable>();
	}
	
	public DrawableBundle( Drawable... drawables ) {
		mChildren = new ArrayList<Drawable>( drawables.length );
		for( Drawable drawable : drawables ) {
			addChild(drawable);
		}
	}
	
	/** adds child at end of children */
	public void addChild(Drawable child) {
		mChildren.add(child);
	}
	
	/** adds child at specified postion (end if position equals number of children) */
	public void addChild(Drawable child, int position) {
		mChildren.add(position, child);
	}

	@Override
	public void draw(GL10 gl) {
		drawChildren(gl);
	}
	
	private void drawChildren(GL10 gl) {
		for(Drawable child : mChildren)
			child.draw(gl);
	}

}
