package com.example.gl.drawable;

import javax.microedition.khronos.opengles.GL10;

import com.example.gl.Color;
import com.example.gl.Drawable;
import com.example.gl.Geometry;
import com.example.gl.Texture;

public class GeometryDrawable implements Drawable
{
	protected Geometry mGeometry;
	private Texture mTexture;
	public static enum ColorMode {
		/** Use whatever is set as the color in the OpenGL state. */
		INHERIT,
		/** Use a single color defined by setColor(). */
		SINGLE_COLOR,
		/** Use per-vertex colors of the geometry, if present */
		VERTEX_COLOR
	}
	private Color mColor = Color.WHITE;
	private ColorMode mColorMode = ColorMode.INHERIT;
	private int mPrimitiveMode = GL10.GL_TRIANGLES;
	
	public GeometryDrawable(Geometry geometry)
	{
		setGeometry(geometry);
		setTexture(null);
	}
	
	public void setPrimitiveMode(int mode) {
		mPrimitiveMode = mode;
	}
	
	public void setTexture(Texture texture) {
		mTexture = texture;
	}
	
	public void useColor(boolean flag) {
		if(flag) {
			setColorMode(ColorMode.SINGLE_COLOR);
		} else {
			setColorMode(ColorMode.INHERIT);
		}
	}
	
	public void setColorMode(ColorMode colorMode) {
		mColorMode = colorMode;
	}
	
	public void setColor(Color color) {
			mColor = color;
	}
	
	protected void setGeometry(Geometry geometry) {
		mGeometry = geometry;
	}
	
	@Override
	public void draw(GL10 gl)
	{
		if(mGeometry == null)
			return;
		
		if(mTexture != null) {
			gl.glEnable(GL10.GL_TEXTURE_2D);
			mTexture.use(gl);
		}
		else
			// TODO: find a better way which does NOT cause side-effects.
			gl.glDisable(GL10.GL_TEXTURE_2D);
		
		switch(mColorMode) {
		case SINGLE_COLOR:
			mColor.set(gl);
			break;
		case VERTEX_COLOR:
			if(mGeometry.getColorValues() != null) {
				gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
				gl.glColorPointer(mGeometry.getColorDimension(), GL10.GL_FLOAT, 0, mGeometry.getColorValues());
			}
			break;
		default:
			break;
		}
		
		if(mGeometry.getVertexCoords() != null)
		{
			gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
			gl.glVertexPointer(mGeometry.getVertexDimension(), GL10.GL_FLOAT, 0, mGeometry.getVertexCoords());
		}
		
		if(mGeometry.getTextureCoords() != null)
		{
			gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);
			gl.glTexCoordPointer(mGeometry.getTextureDimension(), GL10.GL_FLOAT, 0, mGeometry.getTextureCoords());
		}
		
		if(mGeometry.getNormalCoords() != null)
		{
			gl.glEnableClientState(GL10.GL_NORMAL_ARRAY);
			gl.glNormalPointer(GL10.GL_FLOAT, 0, mGeometry.getNormalCoords());
		}
		
		if(mGeometry.getIndices() != null)
		{
			gl.glDrawElements(mPrimitiveMode, mGeometry.getIndexCount(), GL10.GL_UNSIGNED_SHORT, mGeometry.getIndices());
		}
		else
		{
			gl.glDrawArrays(mPrimitiveMode, 0, mGeometry.getVertexCoords().capacity());
		}
		
		if(mColorMode == ColorMode.VERTEX_COLOR) {
			gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
		}
	}

}
