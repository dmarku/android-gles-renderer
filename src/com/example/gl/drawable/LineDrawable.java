package com.example.gl.drawable;

import javax.microedition.khronos.opengles.GL10;

import com.example.gl.geometry.Line;

public class LineDrawable extends GeometryDrawable {

	public LineDrawable(Line line) {
		super(line);
		setPrimitiveMode(GL10.GL_LINES);
	}

}
