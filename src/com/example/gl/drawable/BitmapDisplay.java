package com.example.gl.drawable;

import javax.microedition.khronos.opengles.GL10;

import android.graphics.Bitmap;

import com.example.gl.Color;
import com.example.gl.geometry.Rectangle;
import com.example.gl.texture.BitmapTexture;

/** Displays a Bitmap in a predefined rectangular area */
public class BitmapDisplay extends GeometryDrawable {
	private Rectangle mDisplayArea = new Rectangle(-1, 1, -1, 1, 0);
	private BitmapTexture mTexture = null;
	
	public BitmapDisplay(Bitmap bitmap) {
		super(null);
		
		setBitmap(bitmap);
		
		setGeometry(mDisplayArea);
		setPrimitiveMode(GL10.GL_TRIANGLE_FAN);
		setTexture(mTexture);
		setColor(Color.WHITE);
		useColor(true);
	}

	public void setBitmap(Bitmap bitmap) {
		if(mTexture == null)
			mTexture = new BitmapTexture(bitmap);
		else
			mTexture.setBitmap(bitmap);
	}
	
	public void setArea(float left, float right, float bottom, float top) {
		mDisplayArea.setBoundaries(left, right, bottom, top);
	}
}
