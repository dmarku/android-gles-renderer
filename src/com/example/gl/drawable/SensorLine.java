package com.example.gl.drawable;

import javax.microedition.khronos.opengles.GL10;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.example.gl.Vector;
import com.example.gl.geometry.Line;

/** represents 3D vector as a single, depth-colored line */
public class SensorLine extends GeometryDrawable implements SensorEventListener {
	private SensorManager mSensorManager;
	private Sensor mSensor;
	private float[] mVector;
	private boolean mLandscapeModeEnabled = false;
	private Line mLine = null;
	
	/** landscape mode rotates vector 90 degrees counter-clockwise along z-axis */
	public SensorLine(SensorManager manager, Sensor sensor, boolean landscapeMode) {
		super(null);
		mLine = new Line(new float[]{0,0,0,1}, new float[]{1,1,1,1});
		mGeometry = mLine;
		setSensor(manager, sensor, false);
		setPrimitiveMode(GL10.GL_LINES);
		mVector = new float[] {1, 1, 1};
		mLandscapeModeEnabled = landscapeMode;
	}
	
	/** sets the current sensor whose values are read and registers for events if desired. */
	public void setSensor(SensorManager manager, Sensor sensor, boolean registerForEvents) {
		if(sensor != mSensor) {
			unregister();
			mSensorManager = manager;
			mSensor = sensor;
			if(registerForEvents)
				register();
		}
	}
	
	/** Registers the SensorLine to receive events from the currently set sensor. */
	public void register() {
		if(mSensorManager != null && mSensor != null)
			mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	/** Unregisters the SensorLine from the currently set sensor. */
	public void unregister() {
		if(mSensorManager != null && mSensor != null)
			mSensorManager.unregisterListener(this, mSensor);
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int i) {
		// TODO Auto-generated method stub
		
	}

	private float mSwapVariable = 0f;
	@Override
	public void onSensorChanged(SensorEvent event) {
		Vector.normalize(event.values, mVector);
		if(mLandscapeModeEnabled) {
			mSwapVariable = mVector[0];
			mVector[0] = -mVector[1];
			mVector[1] = mSwapVariable;
		}
		
		mLine.setSecondPoint(mVector);
	}

}
