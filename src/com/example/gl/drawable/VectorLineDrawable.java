package com.example.gl.drawable;

import com.example.gl.ObservableVector;
import com.example.gl.geometry.VectorLine;

public class VectorLineDrawable extends LineDrawable {
	public VectorLineDrawable(ObservableVector vector) {
		super(new VectorLine(vector));
	}
}
