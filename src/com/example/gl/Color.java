package com.example.gl;

import javax.microedition.khronos.opengles.GL10;

public class Color {
	public static float[] RED_ARRAY = {1, 0, 0, 1};
	public static float[] GREEN_ARRAY = {0, 1, 0, 1};
	public static float[] BLUE_ARRAY = {0, 0, 1, 1};
	
	public static float[] WHITE_ARRAY = {1, 1, 1, 1};
	public static float[] GREY_ARRAY = {0.5f, 0.5f, 0.5f, 1};
	public static float[] BLACK_ARRAY = {0, 0, 0, 1};
	
	public static final Color RED     = new Color(1, 0, 0, 1);
	public static final Color GREEN   = new Color(0, 1, 0, 1);
	public static final Color BLUE    = new Color(0, 0, 1, 1);
	
	public static final Color YELLOW  = new Color(1, 1, 0, 1);
	public static final Color CYAN    = new Color(0, 1, 1, 1);
	public static final Color MAGENTA = new Color(1, 0, 1, 1);

	public static final Color WHITE   = new Color(1, 1, 1, 1);
	public static final Color GREY    = new Color(0.5f, 0.5f, 0.5f, 1);
	public static final Color BLACK   = new Color(0, 0, 0, 1);
	
	private float mRed;
	private float mGreen;
	private float mBlue;
	private float mAlpha;
	
	public Color(float red, float green, float blue, float alpha) {
		mRed = red;
		mGreen = green;
		mBlue = blue;
		mAlpha = alpha;
	}
	
	/** set as current color in the given rendering context */
	public void set(GL10 gl) {
		gl.glColor4f(mRed, mGreen, mBlue, mAlpha);
	}
}
