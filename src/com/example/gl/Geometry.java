package com.example.gl;

import java.nio.ByteOrder;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Geometry {
	private FloatBuffer mVertexCoords;
	private FloatBuffer mTextureCoords;
	private FloatBuffer mNormalCoords;
	private FloatBuffer mColorValues;
	
	private ShortBuffer mIndices;
	private int mIndexCount = 0;
	
	/** four floats => one vertex (x, y, z, w) */
	public void setVertexCoords(float... vertexCoords)
	{
		mVertexCoords = storeFloats(vertexCoords, mVertexCoords);
	}
	
	/** two floats => one coordinate (s, t) */
	public void setTextureCoords(float... textureCoords)
	{
		mTextureCoords = storeFloats(textureCoords, mTextureCoords);
	}
	
	/** three floats => one normal (x, y, z) */
	public void setNormalCoords(float... normalCoords)
	{
		mNormalCoords = storeFloats(normalCoords, mNormalCoords);
	}
	
	/** four floats => one color (r, g, b, a) */
	public void setColorValues(float... colorValues) {
		mColorValues = storeFloats(colorValues, mColorValues);
	}
	
	public void setIndices(short... indices)
	{
		mIndices = storeShorts(indices, mIndices);
		mIndexCount = indices.length;
	}
	
	/** four floats => (x, y, z, w) */
	public void setVertexAt(int index, float[] vertex) {
		mVertexCoords.position(index*4);
		if(vertex.length > 4)
			mVertexCoords.put(vertex, 0, 4);
		else 
			mVertexCoords.put(vertex, 0, vertex.length);
		mVertexCoords.position(0);
	}
	
	/**
	 * Tries to store the given float values in the given buffer,
	 * reallocates if neccessary.
	 * @param buffer A preallocated buffer, or null if none is available.
	 * @return The given buffer if it's large enough or a new buffer object
	 *   storing the given values.
	 */
	public static FloatBuffer storeFloats(float[] values, FloatBuffer buffer)
	{
		if(buffer == null || buffer.capacity() <= values.length) {
			buffer = reserveFloats(values.length);
		}
		buffer.put(values);
		buffer.position(0);
		return buffer;
	}
	
	/**
	 * Creates a "direct" FloatBuffer with native byte order for as many floats
	 * as requested by {@code capacity}.
	 * @param capacity The number of float values that need to be stored.
	 * @return An allocated buffer object.
	 */
	private static FloatBuffer reserveFloats(int capacity) {
		ByteBuffer directBuffer = ByteBuffer.allocateDirect(4*capacity);
		directBuffer.order(ByteOrder.nativeOrder());
		return directBuffer.asFloatBuffer();
	}
	
	/**
	 * Tries to store the given short values in the given buffer,
	 * reallocates if neccessary.
	 * @param buffer A preallocated buffer, or null if none is available.
	 * @return The given buffer if it's large enough or a new buffer object
	 *   storing the given values.
	 */
	public static ShortBuffer storeShorts(short[] values, ShortBuffer buffer)
	{
		if(buffer == null || buffer.capacity() >= values.length) {
			buffer = reserveShorts(values.length);
		}
		buffer.put(values);
		buffer.position(0);
		return buffer;
	}
	
	/**
	 * Creates a "direct" ShortBuffer with native byte order for as many shorts
	 * as requested by {@code capacity}.
	 * @param capacity The number of short values that need to be stored.
	 * @return An allocated buffer object.
	 */
	private static ShortBuffer reserveShorts(int capacity) {
		ByteBuffer directBuffer = ByteBuffer.allocateDirect(2*capacity);
		directBuffer.order(ByteOrder.nativeOrder());
		return directBuffer.asShortBuffer();
	}
	
	public FloatBuffer getVertexCoords()
	{
		return mVertexCoords;
	}
	
	public FloatBuffer getTextureCoords()
	{
		return mTextureCoords;
	}
	
	public FloatBuffer getNormalCoords()
	{
		return mNormalCoords;
	}
	
	public FloatBuffer getColorValues() {
		return mColorValues;
	}
	
	public ShortBuffer getIndices()
	{
		return mIndices;
	}
	
	public int getVertexDimension()
	{
		return 4;
	}
	
	public int getTextureDimension()
	{
		return 2;
	}
	
	public int getNormalDimension()
	{
		return 3;
	}
	
	public int getColorDimension() {
		return 4;
	}

	public int getIndexCount() {
		return mIndexCount;
	}
}
