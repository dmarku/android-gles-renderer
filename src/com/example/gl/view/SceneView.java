package com.example.gl.view;

import javax.microedition.khronos.opengles.GL10;

import com.example.gl.Drawable;
import com.example.gl.transform.PerspectiveProjection;

public class SceneView extends ProjectionView {
	private Drawable mRootDrawable = null;
	private float vFov = 90f, mAspect = 1f;
	private float mNear = 1f, mFar = 2f;
	private PerspectiveProjection mProjection = new PerspectiveProjection(-1, 1, -1, 1, 1, 2);

	public SceneView() {
		super();
		setProjection(mProjection);
	}

	@Override
	public void resize(float width, float height) {
		mAspect = width / height;
		mProjection.setByFov(vFov, mAspect, mNear, mFar);
	}
	
	public void setDepth(float near, float far) {
		mNear = near;
		mFar = far;
		mProjection.setByFov(vFov, mAspect, mNear, mFar);
	}
	
	public void setRootDrawable(Drawable rootDrawable) {
		mRootDrawable = rootDrawable;
	}

	@Override
	protected void onDraw(GL10 gl) {
		gl.glEnable(GL10.GL_DEPTH_TEST);
		
		if(mRootDrawable != null) {
			mRootDrawable.draw(gl);
		}
		
		gl.glDisable(GL10.GL_DEPTH_TEST);
	}

}
