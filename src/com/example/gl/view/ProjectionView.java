package com.example.gl.view;

import javax.microedition.khronos.opengles.GL10;

import com.example.gl.RenderView;
import com.example.gl.Transform;

public abstract class ProjectionView implements RenderView {
	protected Transform mProjection = null;

	protected ProjectionView() {
	}
	
	public ProjectionView(Transform projection) {
		mProjection = projection;
	}
	
	protected void setProjection(Transform projection) {
		mProjection = projection;
	}
	
	protected abstract void onDraw(GL10 gl); 

	@Override
	public void draw(GL10 gl) {
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glPushMatrix();
		gl.glMultMatrixf(mProjection.getMatrix(), 0);
		
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		onDraw(gl);
		
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glPopMatrix();
		
		gl.glMatrixMode(GL10.GL_MODELVIEW);
	}

}
