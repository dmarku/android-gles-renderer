package com.example.gl;

import com.example.gl.ObservableVector.VectorChangeListener;

import android.hardware.Sensor;
import android.hardware.SensorManager;

public class SensorMatrix extends Matrix {
	
	private SensorVector mGravityVector;
	private SensorVector mMagnetismVector;
	
	public SensorMatrix(SensorManager manager, boolean landscapeMode) {
        Sensor gravitySensor = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor magnetismSensor = manager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        
        mGravityVector = new SensorVector(manager, gravitySensor, landscapeMode);
        mMagnetismVector = new SensorVector(manager, magnetismSensor, landscapeMode);
        
        mMagnetismVector.addListener(new VectorChangeListener() {
			
			@Override
			public void onVectorChange(Vector vector) {
				Vector.ortho(vector.getValues(), mGravityVector.getValues(), vector.getValues());
				Vector.normalize(vector.getValues(), vector.getValues());
				SensorManager.getRotationMatrix(
						getValues(),
						null,
						mGravityVector.getValues(),
						mMagnetismVector.getValues());
			}
		});
        
        
        mGravityVector.addListener(new VectorChangeListener() {
			
			@Override
			public void onVectorChange(Vector vector) {
				SensorManager.getRotationMatrix(
						getValues(),
						null,
						mGravityVector.getValues(),
						mMagnetismVector.getValues());
			}
		});
		
	}
	
	public void register(int rate) {
    	mGravityVector.register(rate);
    	mMagnetismVector.register(rate);
	}
	
	public void unregister() {
    	mGravityVector.unregister();
    	mMagnetismVector.unregister();
	}
}
