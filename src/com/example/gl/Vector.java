package com.example.gl;

/** Utility functions for vector maths */
public class Vector {
	private final int mDimension;
	private float[] mValues;
	
	/**
	 * Creates a Vector of the given dimension and with the given values
	 * @throws IllegalArgumentException when the number of supplied values is 
	 *   smaller than the given dimension
	 */
	public Vector(int dimension, float... values) throws IllegalArgumentException {
		if(dimension <= 0)
			throw new IllegalArgumentException("Dimension must be greater than zero.");
		if(values.length < dimension)
			throw new IllegalArgumentException("At least as many values as the dimension must be given.");
		
		mDimension = dimension;
		mValues = values.clone();
	}
	
	/**
	 * Creates a Vector of a dimension given by the value array.
	 * @param values
	 */
	public Vector(float... values) {
		mDimension = values.length;
		mValues = values.clone();
	}
	
	/**
	 * Creates a Vector of the given dimension, initialized with zero
	 * @throws IllegalArgumentException
	 */
	public Vector(int dimension) throws IllegalArgumentException {
		if(dimension <= 0)
			throw new IllegalArgumentException("Dimension must be greater than zero.");
			
		mDimension = dimension;
		mValues = new float[dimension];
		for(int i = 0; i < mValues.length; ++i) {
			mValues[i] = 0f;
		}
	}
	
	/**
	 * Sets the vector coordinates to the given values
	 * @param values
	 * @throws IllegalArgumentException if the number of given values does not
	 *   match the vector dimension.
	 */
	public void set(float... values) throws IllegalArgumentException {
		if(values.length != mDimension) {
			throw new IllegalArgumentException(
					"The number of given values must fit the dimension of the Vector.");
		}
		for(int i = 0; i < mValues.length; ++i)
			mValues[i] = values[i];
	}
	
	/**
	 * Returns the array of values backing this vector. <b>This is not a copy!</b>
	 * @return
	 */
	public float[] getValues() {
		return mValues;
	}
	
	public static final String INEQUAL_LENGTH_ERROR_MESSAGE = "Vector dimensions are not equal.";
	
	/**
	 * Normalizes the given vector and stores the normalized vector in
	 * {@code result}.
	 * 
	 * @throws IllegalArgumentException if the dimensions of the given vectors
	 *   do not match.
	 */
	public static void normalize(float[] vector, float[] result)
			throws IllegalArgumentException {
		checkDimensions(vector, result);
		
		float squareSum = 0;
		for(float coordinate : vector) {
			squareSum += coordinate*coordinate;
		}
		float length = (float) Math.sqrt(squareSum);
		float rlength = (Math.abs(length) <= 0.001) ? 1 : 1/length;
		
		for(int i = 0; i < vector.length; ++i) {
			result[i] = vector[i]*rlength;
		}
	}
	
	/**
	 * Subtracts one vector from another and stores the difference in
	 * {@code result}.
	 * 
	 * @throws IllegalArgumentException if the dimensions of the given vectors
	 *   do not match.
	 */
	public static void sub(float[] first, float[] second, float[] result)
			throws IllegalArgumentException {
		checkDimensions(first, second, result);
		
		for(int i = 0; i < first.length; ++i) {
			result[i] = first[i] - second[i];
		}
	}
	
	/**
	 * Calculates the dot product of two vectors. 
	 * @throws IllegalArgumentException if the dimensions of the given vectors
	 *   do not match.
	 */
	public static float dot(float[] first, float[] second)
			throws IllegalArgumentException {
		checkDimensions(first, second);
			
		float sum = 0f;
		for(int i = 0; i < first.length; ++i) {
			sum += first[i] * second[i];
		}
		return sum;
	}
	
	/** Orthogonalizes the given vector to a reference vector */
	public static void ortho(float[] vector, float[] reference, float[] result)
			throws IllegalArgumentException {
		checkDimensions(vector, reference, result);
		
		float coefficient = dot(reference, vector)/dot(reference, reference);
		for(int i = 0; i < vector.length; ++i) {
			result[i] = vector[i] - coefficient * reference[i];
		}
	}
	
	private static void checkDimensions(float[]... vectors)
			throws IllegalArgumentException{
		
		if(vectors.length > 1) {
			
			int firstLength = vectors[0].length;
			for(float[] vector : vectors) {
				
				if(vector.length != firstLength) {
					throw new IllegalArgumentException(INEQUAL_LENGTH_ERROR_MESSAGE);
				}
			}
		}
	}
}
