package com.example.gl;

import javax.microedition.khronos.opengles.GL10;

public interface Texture {
	/** must be called once to allocate a texture ID */
//	public void init(GL10 gl);
	/**
	 * use this to bind the texture to the current context and perform any
	 * other necessary tasks before usage.
	 */
	public void use(GL10 gl);
}