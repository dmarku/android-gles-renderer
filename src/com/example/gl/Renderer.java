package com.example.gl;

import java.util.ArrayList;
import java.util.List;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.util.Log;

public class Renderer implements android.opengl.GLSurfaceView.Renderer {
	private static final String TAG = "Renderer";
	
	private float[] mClearColor = {0, 0, 0.2f, 1};
	private float mSurfaceWidth = 0, mSurfaceHeight= 0;
	
	private List<RenderView> mRenderViews;

	public Renderer() {
		mRenderViews = new ArrayList<RenderView>(1);
	}
	
	/** append RenderView to existing list */
	public void addRenderView(RenderView renderView) {
		mRenderViews.add(renderView);
	}
	
	/**
	 * insert RenderView at specific position, append if position equals number
	 * of existing RenderViews.
	 */
	public void addRenderView(RenderView renderView, int position) {
		renderView.resize(mSurfaceWidth, mSurfaceHeight);
		mRenderViews.add(position, renderView);
	}
	
	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		Log.i(TAG, "Rendering surface created. Setting clear color and initial matrices.");
		gl.glLineWidth(1f);
		
		gl.glClearColor(mClearColor[0], mClearColor[1], mClearColor[2], mClearColor[3]);
		
		gl.glMatrixMode(GL10.GL_PROJECTION);
		gl.glLoadIdentity();
		gl.glMatrixMode(GL10.GL_MODELVIEW);
		gl.glLoadIdentity();
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		Log.i(TAG, "Rendering surface has changed. Forwarding new size to RenderViews");
		
		mSurfaceWidth = (float)width;
		mSurfaceHeight = (float)height;
		for(RenderView renderView : mRenderViews)
			renderView.resize(mSurfaceWidth, mSurfaceHeight);
	}
	
	@Override
	public void onDrawFrame(GL10 gl) {
		gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		
		for(RenderView renderView : mRenderViews)
			renderView.draw(gl);
	}
	
	protected float getWidth() {
		return mSurfaceWidth;
	}
	
	protected float getHeight() {
		return mSurfaceHeight;
	}
}
