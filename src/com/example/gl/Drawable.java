package com.example.gl;

import javax.microedition.khronos.opengles.GL10;

public interface Drawable {
	public void draw(GL10 gl);
}
