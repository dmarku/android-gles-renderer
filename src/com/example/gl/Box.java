package com.example.gl;

/**
 * (Bounding) Box enclosing a 3D space.
 * 
 * The Box is enclosed by six planes, all of them axis-aligned. The planes are
 * defined by distances from the origin along the x- (left, right), y- (bottom,
 * top) and z-axis (front, rear).
 */

public class Box {
	private float mLeft, mRight, mBottom, mTop, mRear, mFront;
	
	public void set(
			float left, float right,
			float bottom, float top,
			float rear, float front) {
		
		setWidthBoundaries(left, right);
		setHeightBoundaries(bottom, top);
		setDepthBoundaries(rear, front);
	}
	
	public void setWidthBoundaries(float left, float right) {
		mLeft = left;
		mRight = right;
	}
	
	public void setHeightBoundaries(float bottom, float top) {
		mBottom = bottom;
		mTop = top;
	}
	
	public void setDepthBoundaries(float rear, float front) {
		mRear = rear;
		mFront = front;
	}
	
	public float left() {
		return mLeft;
	}
	
	public float right() {
		return mRight;
	}
	
	public float bottom() {
		return mBottom;
	}
	
	public float top() {
		return mTop;
	}
	
	public float front() {
		return mFront;
	}
	
	public float rear() {
		return mRear;
	}
	
	public float getWidth() {
		return mRight - mLeft;
	}
	
	public float getHeight() {
		return mTop - mBottom;
	}
	
	public float getDepth() {
		return mFront - mRear;
	}
}
