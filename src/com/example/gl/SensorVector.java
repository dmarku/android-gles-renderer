package com.example.gl;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorVector extends ObservableVector implements SensorEventListener {
	private SensorManager mSensorManager = null;
	private Sensor mSensor = null;
	private boolean mLandscapeMode = false;
	private float[] mNormalizedValues;
	
	public SensorVector(SensorManager manager, Sensor sensor, boolean landscapeMode) {
		super(3);
		setSensor(manager, sensor);
		mLandscapeMode = landscapeMode;
		mNormalizedValues = new float[3];
	}
	
	public void setSensor(SensorManager manager, Sensor sensor) {
		if(sensor != mSensor) {
			unregister();
			mSensorManager = manager;
			mSensor = sensor;
		}
	}
	
	public void register(int rate) {
		if(mSensorManager != null && mSensor != null) {
			mSensorManager.registerListener(this, mSensor, rate);
		}
	}
	
	public void unregister() {
		if(mSensorManager != null && mSensor != null) {
			mSensorManager.unregisterListener(this, mSensor);
		}
	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		Vector.normalize(event.values, mNormalizedValues);
		if(mLandscapeMode) {
			float y = mNormalizedValues[1];
			mNormalizedValues[1] = mNormalizedValues[0];
			mNormalizedValues[0] = -y;
		}
		set(mNormalizedValues);
	}
}
