package com.example.gl.geometry;

import com.example.gl.ObservableVector;
import com.example.gl.Vector;

public class VectorLine extends Line implements ObservableVector.VectorChangeListener {

	public VectorLine(ObservableVector vector) {
		super(new float[]{0, 0, 0, 1}, new float[]{1, 1, 1, 1});
		vector.addListener(this);
	}

	@Override
	public void onVectorChange(Vector vector) {
		setSecondPoint(vector.getValues());
	}
}
