package com.example.gl.geometry;

import com.example.gl.Geometry;

/** xy-Grid, centered. USE GL_LINES PRIMITIVE MODE WHEN DRAWING! */
public class Grid extends Geometry {
	public Grid(int cols, int rows, float tileSize) {
		
		float totalWidth = cols * tileSize;
		float totalHeight = rows * tileSize;
		
		int pointCount = (cols+1) * (rows+1);
		float[] pointCoords = new float[pointCount * 4];
		
		float x;
		float y = -0.5f * totalHeight;
		
		int index = 0;
		
		for(int row = 0; row <= rows; ++row) {
			x = -0.5f * totalWidth;
			for(int column = 0; column <= cols; ++column) {
				pointCoords[index++] = x;
				pointCoords[index++] = y;
				pointCoords[index++] = 0.0f;
				pointCoords[index++] = 1.0f;
				
				x += tileSize;
			}
			y += tileSize;
		}
		
		setVertexCoords(pointCoords);
		
		int lineCount = 2*cols*rows + cols + rows;
		short[] indices = new short[2*lineCount];
		
		int indexIndex = 0;
		short pointIndex = 0;
		
		// first: all horizontal lines
		// i.e. iterate over all row borders and in each along columns
		
		for(int row = 0; row <= rows; ++row) {	
			for(int column = 0; column < cols; ++column) {
				indices[indexIndex++] = pointIndex;
				indices[indexIndex++] = ++pointIndex;
			}
			++pointIndex;
		}
		
		// second: all vertical lines
		// i.e. iterate over all rows and in each along column borders
		
		pointIndex = 0;
		for(int row = 0; row < rows; ++row) {
			for(int column = 0; column <= cols; ++column) {
				indices[indexIndex++] = pointIndex;
				indices[indexIndex++] = (short)(pointIndex+cols+1);
				++pointIndex;
			}
		}
		
		setIndices(indices);
	}
}
