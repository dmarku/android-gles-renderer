package com.example.gl.geometry;

import com.example.gl.Geometry;

/**
 * An axis-aligned rectangle in the xy-plane.
 * Vertices defined counter-clockwise
 */
public class Rectangle extends Geometry {
	private float mZ = 0f;
	
	public Rectangle() {
		setBoundaries(0, 0, 0, 0);
		setNormalCoords(
				0,0,1,
				0,0,1,
				0,0,1,
				0,0,1);
		setTextureCoords(0,1, 1,1, 1,0, 0,0);
		setIndices(new short[]{0,1,2,3});
	}
	
	public Rectangle(float left, float right, float bottom, float top, float z) {
		setBoundaries(left, right, bottom, top);
		setNormalCoords(
				0,0,1,
				0,0,1,
				0,0,1,
				0,0,1);
		setTextureCoords(0,1, 1,1, 1,0, 0,0);
		setIndices(new short[]{0,1,2,3});
	}
	
	public void setBoundaries(float left, float right, float bottom, float top) {
		setVertexCoords(
				left, bottom, mZ, 1,
				right, bottom, mZ, 1,
				right, top, mZ, 1,
				left, top, mZ, 1);
	}
	
	public void setZ(float z) {
		mZ = z;
	}
}
