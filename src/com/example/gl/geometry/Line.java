package com.example.gl.geometry;

import com.example.gl.Geometry;

public class Line extends Geometry {
	public Line(float[] firstPoint, float[] secondPoint) {
		setVertexCoords(
				firstPoint[0], firstPoint[1], firstPoint[2], 1f,
				secondPoint[0], secondPoint[1], secondPoint[2], 1f);
		setIndices((short)0, (short)1);
	}
	
	public void setFirstPoint(float[] point) {
		setVertexAt(0, point);
	}
	
	public void setSecondPoint(float[] point) {
		setVertexAt(1, point);
	}
}
