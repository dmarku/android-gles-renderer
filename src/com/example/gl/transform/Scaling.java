package com.example.gl.transform;

import com.example.gl.Transform;

public class Scaling implements Transform {
	private float[] mMatrix = new float[] {
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
	};
	
	public Scaling(float globalScale) {
		setScale(globalScale);
	}
	
	public void setScale(float globalScale) {
		mMatrix[0] = globalScale;
		mMatrix[5] = globalScale;
		mMatrix[10] = globalScale;
	}

	@Override
	public float[] getMatrix() {
		// TODO Auto-generated method stub
		return mMatrix;
	}

}
