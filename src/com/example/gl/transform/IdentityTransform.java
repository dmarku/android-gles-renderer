package com.example.gl.transform;

import com.example.gl.Transform;

public class IdentityTransform implements Transform {
	public static float[] mIdentityMatrix = {
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1
	};
	
	public float[] getMatrix()
	{
		return mIdentityMatrix;
	}
}
