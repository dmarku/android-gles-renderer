package com.example.gl.transform;

import com.example.gl.Box;
import com.example.gl.Transform;
import com.example.gl.Vector;

public class OrthoProjection implements Transform {
	private float[] mMatrix;
	private Box mClipVolume;

	public OrthoProjection(
			float left, float right,
			float bottom, float top,
			float near, float far) {
		
		mClipVolume = new Box();
		
		mMatrix = new float[] {
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, -1, 0,
				0, 0, 0, 1
		};
		setBoundaries(left, right, bottom, top, near, far);
	}
	
	public void setBoundaries(
			float left, float right,
			float bottom, float top,
			float near, float far) {
		
		setWidthBoundaries(left, right);
		setHeightBoundaries(bottom, top);
		setDepthBoundaries(near, far);
	}
	
	public void setWidthBoundaries(float left, float right) {
		
		mClipVolume.setWidthBoundaries(left, right);
		
		float w = right-left;
		mMatrix[0]  =  2f/w;
		mMatrix[12] = -(right+left)/w;
	}
	
	public void setHeightBoundaries(float bottom, float top) {
		
		mClipVolume.setHeightBoundaries(bottom, top);
		
		float h = top-bottom;
		mMatrix[5]  =  2f/h;
		mMatrix[13] = -(top+bottom)/h;
	}
	
	public void setDepthBoundaries(float near, float far) {
		
		mClipVolume.setDepthBoundaries(far, near);
		
		float d = far-near;
		mMatrix[10] = -2f/d;
		mMatrix[14] = -(far+near)/d;
	}
	
	@Override
	public float[] getMatrix() {
		return mMatrix;
	}

	public Vector getFrontPoint(float x, float y) {
		float px = (1.f - x) * mClipVolume.left() + x * mClipVolume.right();
		float py = (1.f - y) * mClipVolume.bottom() + x * mClipVolume.top();
		return new Vector(px, py, mClipVolume.front());
	}
	
	public Vector getDirection(float x, float y) {
		return new Vector(0.f, 0.f, -1.f);
	}
}
