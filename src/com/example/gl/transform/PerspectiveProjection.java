package com.example.gl.transform;

import com.example.gl.Transform;

public class PerspectiveProjection implements Transform {
	private float[] mMatrix;
	private float mNear, mWidth, mHeight;
	
	public PerspectiveProjection(
			float left, float right,
			float bottom, float top,
			float near, float far) {
		
		mNear = near;
		
		mMatrix = new float[] {
				1, 0, 0, 0,
				0, 1, 0, 0,
				0, 0, -3, -1,
				0, 0, -4, 0 };
		setBoundaries(left, right, bottom, top, near, far);
	}
	
	/**
	 * Sets the view frustum from given vertical field of view and view aspect.
	 * @param radFov Vertical field of view in degree.
	 * @param aspect View aspect (width/height ratio).
	 * @param near Distance of the near clipping plane from the view point.
	 * @param far Distance of the far clipping plane from the view point.
	 */
	public void setByFov(float fov, float aspect, float near, float far) {
		setByRadFov((float) Math.toRadians(fov), aspect, near, far);
	}
	
	public void setByRadFov(float radFov, float aspect, float near, float far) {
		float top = near * (float) Math.tan(.5f*radFov);
		setBoundaries(-aspect*top, aspect*top, -top, top, near, far);
	}
	
	public void setBoundaries(
			float left, float right,
			float bottom, float top,
			float near, float far) {
		setWidthBoundaries(left, right);
		setHeightBoundaries(bottom, top);
		setDepthBoundaries(near, far);
	}
		
	public void setWidthBoundaries(float left, float right) {
		mWidth = right - left;
		mMatrix[0] = 2*mNear / mWidth;
		mMatrix[8] = (right+left) / mWidth;
	}
	
	public void setHeightBoundaries(float bottom, float top) {
		mHeight = top - bottom;
		mMatrix[5] = 2*mNear / mHeight;
		mMatrix[9] = (top+bottom) / mHeight;
	}
	
	public void setDepthBoundaries(float near, float far) {
		mNear = near;
		float depth = far - near;
		
		mMatrix[0] = 2*near / mWidth;
		mMatrix[5] = 2*near / mHeight;
		
		mMatrix[10] = -(far+near) / depth;
		mMatrix[14] = -2 * far * near / depth;
	}
	
	@Override
	public float[] getMatrix() {
		return mMatrix;
	}
}
