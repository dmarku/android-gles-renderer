package com.example.gl.transform;

import com.example.gl.Transform;

public class Translation implements Transform {
	private float[] mMatrix = new float[] {
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
	};

	public Translation() {
	}
	
	public Translation(float dx, float dy, float dz) {
		set(dx, dy, dz);
	}
	
	public void set(float dx, float dy, float dz) {
		mMatrix[12] = dx;
		mMatrix[13] = dy;
		mMatrix[14] = dz;
	}
	
	@Override
	public float[] getMatrix() {
		return mMatrix;
	}
	
	public float getX() { return mMatrix[12]; };
	public float getY() { return mMatrix[13]; };
	public float getZ() { return mMatrix[14]; };
	
	public void setX(float x) { mMatrix[12] = x; };
	public void setY(float y) { mMatrix[13] = y; };
	public void setZ(float z) { mMatrix[14] = z; };

}
