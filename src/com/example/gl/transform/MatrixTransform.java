package com.example.gl.transform;

import com.example.gl.Matrix;
import com.example.gl.Transform;

public class MatrixTransform implements Transform {
	private Matrix mMatrix;

	public MatrixTransform(Matrix matrix) {
		mMatrix = matrix;
	}
	
	@Override
	public float[] getMatrix() {
		return mMatrix.getValues();
	}

	
}
