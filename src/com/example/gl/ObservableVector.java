package com.example.gl;

import java.util.ArrayList;
import java.util.List;

public class ObservableVector extends Vector {
	
	private List<VectorChangeListener> mListeners = null;
	
	public interface VectorChangeListener {
		public void onVectorChange(Vector vector);
	}
	

	public ObservableVector(int dimension) throws IllegalArgumentException {
		super(dimension);
		mListeners = new ArrayList<ObservableVector.VectorChangeListener>();
	}
	
	public ObservableVector(int dimension, float... values) throws IllegalArgumentException {
		super(dimension, values);
		mListeners = new ArrayList<ObservableVector.VectorChangeListener>();
	}
	
	public void addListener(VectorChangeListener listener) {
		mListeners.add(listener);
	}
	
	public void removeListener(VectorChangeListener listener) {
		mListeners.remove(listener);
	}
	
	@Override
	public void set(float... values) {
		super.set(values);
		for(VectorChangeListener listener : mListeners) {
			listener.onVectorChange(this);
		}
	}
}
